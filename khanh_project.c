//Tran Duy Khanh EEEEIU20031
//Final Project Programming C for Engineer

//Include library and project header
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"project.h"
#define SIZE 1000

int main(){
	//Declare option variable
	unsigned option;
	
	//Declare number of student(s) in the system
	static int n = 0;
	
	//Declare pointer pointing to structure student
	student  *s;
	//Dynamic Memory Allocation for array student s
	s = (student *) malloc(SIZE* sizeof(student));
	
	//Display School Management System
	printf("-------------------------------------------------------------\n");
	printf("\tSchool Management System\n");
	printf("-------------------------------------------------------------\n");
	
	option_display: //Label to go back options table
	//All valid options which can be choosen by user
	printf("Enter <1> to Add new student\n");
	printf("Enter <2> to Display all student\n");
	printf("Enter <3> to Remove student\n");
	printf("Enter <4> to Edit student\n");
	printf("Enter <5> to Display the options again\n");
	printf("Enter <0> to Exit\n");
	
	do{
		do{
			//Input options
			printf("\nEnter you choice: ");
			scanf("%u", &option);
			
			//Require the input option is from options table
			if(option<0 || option>5)
				printf("Invalid option. ");
				
			//Looping until the valid option is typed
		}while(option<0 || option>5);
		
		//Split case for specific option
		switch(option){
			case 1:{
				//The function accepts array s and number of student
				//and return new number of student
				n = add_student(s,n);
				break;
			}
			case 2:{
				//The function display all stored students datas
				display_student(s,n);
				break;
			}
			case 3:{
				//The function accepts array s and number of student
				//and return new number of student 
				//if the the removed data is found in the system
				n = remove_student(s,n);
				break;
			}
			case 4:{
				//The function changes needed data
				edit_student(s,n);
				break;
			}
			case 5:{
				//Go back to options table
				goto option_display;
				break;
			}
			case 0:{
				//Indicate that the program end
				printf("End program.\n");
				break;
			}
		}
		
	}while(option<=5 && option>0);
	
	return 0;
}


