## School Management Program in C Language
The program provides various function to manage student data in C language. Some functions include: add, modify base on student ID, remove student. The project apply structure, dynamic allocation for data storage.

## Result
# Program Terminal Console
![program console](/result/program_exe.png)
# Structure and functions
![structure data](/result/struct_student.png)
![display student](/result/display_student.png)
![add student](/result/add_student.png)
![delete student](/result/delete_student.png)

