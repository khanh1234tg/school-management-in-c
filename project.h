//Tran Duy Khanh EEEEIU20031
//project header


//define structure student
typedef struct{
	char name[51]; //Name with 50 characters
	char ID[16]; //ID with 15 characters
	char branch[6]; //branch with 5 characters
}student; //Name of the structure

//Functions definition
int add_student(student *s, int i){
	
	//demand variable to continue input student
	char demand;
	
	do{
		do{
			//Enter student information
			fflush(stdin);
			printf("Enter name: ");
			gets(s[i].name);
			printf("Enter ID: ");
			scanf("%s", s[i].ID);
			printf("Enter branch: ");
			scanf("%s", s[i].branch);
			printf("\n");
		//Check condition to continue looping
		}while(check_input(s,i));	
		
		//Print out typed information
		printf("%-25s%-20s%-10s\n", "Name", "ID", "Branch");
		printf("-------------------------------------------------------------\n");
		printf("-------------------------------------------------------------\n");
		printf("%-25s%-20s%-10s\n", s[i].name, s[i].ID, s[i].branch);
		i++; //Increment for next data in the array
		//Condition to continue entering new data
		printf("\nPress [Y] to enter more: ");
		scanf(" %c", &demand);
		
	}while(demand == 'Y');
	
	//Return new number of students to the main program
	return i;
}
void display_student(student *s, const int n){
	int i;
	//Display stored information
	printf("%-25s%-20s%-10s\n", "Name", "ID", "Branch");
	printf("-------------------------------------------------------------\n");
	printf("-------------------------------------------------------------\n");
	
	//Display all students in the system
	for(i=0;i<n;i++)
		printf("%-25s%-20s%-10s\n", s[i].name, s[i].ID, s[i].branch);
	
	printf("\n");
}
int remove_student(student *s, int n){
	//Declare counter i, removed position of data
	int i, position = -1;
	char ID[16];
	
	//Enter ID to delete
	printf("Enter ID of Student for removing: ");
	scanf("%s", ID);
	
	for(i=0;i<n;i++)
		//Check removed ID in the system
		if(!strcmp((s+i)->ID,ID)){
			
			printf("Student %s - %s is already deleted\n",(s+i)->name, (s+i)->ID);
			position = i; //Found the position of removed data
			
			//Push the removed data out of the array
			for(i=position;i<n-1;i++){
				strcpy((s+i)->name,(s+i+1)->name);
				strcpy((s+i)->ID,(s+i+1)->ID);
				strcpy((s+i)->branch,(s+i+1)->branch);
			}		
		}
	
	//Decrement number of students
	//when the removed data is found in the system
	if(n>=1 && position != -1)
		n--;
		
	//Else the number of students remain the same
	else
		printf("The ID is not found in the system\n");
		
	//Return number of students to the main program		
	return n;	
}
void edit_student(student *s, const int n){
	//Declare counter, indicator sucess
	int i, sucess = 0;
	char ID[16];
	
	printf("Enter ID of Student for editing: ");
	scanf("%s", ID);
	
	for(i=0;i<n;i++)
		if(!strcmp((s+i)->ID,ID)){
			//Let sucess = 1 to indicate
			//that the edited information is found
			sucess = 1;
			fflush(stdin);
			printf("Enter name: ");
			gets((s+i)->name);
			printf("Enter branch: ");
			scanf("%s", (s+i)->branch);
			
			//Print out edited data
			printf("\n%-25s%-20s%-10s\n", "Name", "ID", "Branch");
			printf("-------------------------------------------------------------\n");
			printf("-------------------------------------------------------------\n");
			printf("%-25s%-20s%-10s\n", s[i].name, s[i].ID, s[i].branch);
		}
	
	//Condition where the needed ID
	//is not found in the system
	if(sucess!=1)
		printf("The ID is not found in the system\n");
	
	//Else the procedure is succeeded
	else
		printf("\nThe data has been modified\n");
}

int check_input(student *s, const int n){
	/*The check_input function is used in add_student function*/
	
	//declare counter i
	int i, flag = 0;
	//If number of student is less than 2
	//The array can not have same ID
	//and return 0
	if(n==0)
		return 0;
	
	else{
		//Check whether the new input data
		//has the same ID as stored data in the system
		for(i=0;i<=n-1;i++)
			//If yes, return 1 to the function add_student
			if(!strcmp(s[i].ID,s[n].ID)){
				printf("THIS ID ALREADY EXISTS!!! PLEASE ENTER AGAIN\n");
				return 1;
				
				//Set flag = 1 to indicate that the ID is found
				flag = 1;
			}	
	}
	
	//If ID is not found return 0 to function add_student
	if(flag != 1)
		return 0;
}
